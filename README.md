# README #

This README will help with setting up the project and running it.

### What is this repository for? ###

* This repository is used for the CIS2232 Deep Sea Fishing Project
* Version 6.6.6

### How do I get set up? ###

* Clone Project to local drive
* Open project in netbeans
* Run Xampp and start Apache and 
* Withing program access OtherSources/src/main/resources/db.mysql/richardsdeepseafishing.sql
* Run this Sql file in PhpMyAdmin
* Deploy project and access functionality

### Contribution guidelines ###

* Tests included are calculate price test
* any code altered shall be documented accordingly

### Who do I talk to? ###

* Ben Bain, Brodie MacBeath, Stephane Levesque
* Contact through Holland College.