package info.dsf.data.model;

import java.util.ResourceBundle;

/**
 * Purpose: To allow functionality of an database connection.
 */
public class DatabaseConnection {

    private String databaseName;
    private String userName;
    private String password;

    //Contsructors
    public DatabaseConnection(String databaseName, String userName, String password) {
        this.databaseName = databaseName;
        this.userName = userName;
        this.password = password;
    }

    public DatabaseConnection() {
        try {
            String propFileName = "spring.data-access";
            ResourceBundle rb = ResourceBundle.getBundle(propFileName);
            userName = rb.getString("jdbc.username");
            password = rb.getString("jdbc.password");
            databaseName = rb.getString("jdbc.dbname");
            System.out.println("BJM-Set the database to " + databaseName);
        } catch (Exception ex) {
            System.out.println("*** ERROR CONNECTING TO DATABASE ***"
                    + "\nREASON: " + ex.toString());
        }
    }

    //Accessor Methods
    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "DatabaseConnection{" + "databaseName=" + databaseName + ", userName=" + userName + ", password=" + password + '}';
    }

}
