package info.dsf.data.jparepository;

import info.dsf.data.model.Customers;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Purpose: To allow JPA functionality with the customers table.
 */
@Repository
public interface CustomersRepository  extends CrudRepository<Customers, Integer> {
    
    @Override
    public ArrayList<Customers> findAll();
    
    public ArrayList<Customers> findByEmail(String email);
}
