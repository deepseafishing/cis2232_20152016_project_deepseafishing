package info.dsf.data.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "catchlog")
public class Catchlog implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "dateOfTrip")
    @Temporal(TemporalType.DATE)
    private Date dateOfTrip;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 3)
    @Column(name = "timeOfTrip")
    private String timeOfTrip;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "mackerelCount")
    private int mackerelCount;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "codCount")
    private int codCount;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "sculpinCount")
    private int sculpinCount;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "halibutCount")
    private int halibutCount;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "lobsterCount")
    private int lobsterCount;
    
    @Size(max = 80)
    @Column(name = "notes")
    private String notes;

    public Catchlog() {
    }

    public Catchlog(Integer id) {
        this.id = id;
    }

    public Catchlog(Date dateOfTrip, String timeOfTrip, String notes, int mackerelCount, int codCount, int sculpinCount, int halibutCount, int lobsterCount) {
        this.dateOfTrip = dateOfTrip;
        this.timeOfTrip = timeOfTrip;
        this.notes = notes;
        this.mackerelCount = mackerelCount;
        this.codCount = codCount;
        this.sculpinCount = sculpinCount;
        this.halibutCount = halibutCount;
        this.lobsterCount = lobsterCount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateOfTrip() {
        return dateOfTrip;
    }

    public void setDateOfTrip(Date dateOfTrip) {
        this.dateOfTrip = dateOfTrip;
    }

    public String getTimeOfTrip() {
        return timeOfTrip;
    }

    public void setTimeOfTrip(String timeOfTrip) {
        this.timeOfTrip = timeOfTrip;
    }

    public int getMackerelCount() {
        return mackerelCount;
    }

    public void setMackerelCount(int mackerelCount) {
        this.mackerelCount = mackerelCount;
    }

    public int getCodCount() {
        return codCount;
    }

    public void setCodCount(int codCount) {
        this.codCount = codCount;
    }

    public int getSculpinCount() {
        return sculpinCount;
    }

    public void setSculpinCount(int sculpinCount) {
        this.sculpinCount = sculpinCount;
    }

    public int getHalibutCount() {
        return halibutCount;
    }

    public void setHalibutCount(int halibutCount) {
        this.halibutCount = halibutCount;
    }

    public int getLobsterCount() {
        return lobsterCount;
    }

    public void setLobsterCount(int lobsterCount) {
        this.lobsterCount = lobsterCount;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
    
    @Override
    public String toString() {
        return "info.dsf.data.model.Catchlog[ id=" + id + " ]";
    }
}
