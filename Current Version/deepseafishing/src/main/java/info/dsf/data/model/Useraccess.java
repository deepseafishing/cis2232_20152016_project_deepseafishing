package info.dsf.data.model;

import info.dsf.utils.SeaUtil;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * An generated entity class for the user access table.
 */
@Entity
@Table(name = "UserAccess")
public class Useraccess implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "userAccessId")
    private Integer userAccessId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "username")
    private String username;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 128)
    @Column(name = "password")
    private String password;
    
    @Transient
    private String noHashPassword;
    
    @Column(name = "createdDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDateTime = SeaUtil.setDate();
    
    @JoinColumn(name = "userTypeCode", referencedColumnName = "codeValueSequence")
    @ManyToOne(optional = false)
    private Codevalue userTypeCode;
    
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "useraccess")
    private Customers customers;

    public Useraccess() {
    }

    public Useraccess(Integer userAccessId) {
        this.userAccessId = userAccessId;
    }

    public Useraccess(Integer userAccessId, String username, String password) {
        this.userAccessId = userAccessId;
        this.username = username;
        this.password = password;
    }

    public Integer getUserAccessId() {
        return userAccessId;
    }

    public void setUserAccessId(Integer userAccessId) {
        this.userAccessId = userAccessId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        setNoHashPassword(password);
        this.password = SeaUtil.getHashPassword(password);
    }

    public String getNoHashPassword() {
        return noHashPassword;
    }

    public void setNoHashPassword(String noHashPassword) {
        this.noHashPassword = noHashPassword;
    }
    
    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Codevalue getUserTypeCode() {
        return userTypeCode;
    }

    public void setUserTypeCode(Codevalue userTypeCode) {
        this.userTypeCode = userTypeCode;
    }

    public Customers getCustomers() {
        return customers;
    }

    public void setCustomers(Customers customers) {
        this.customers = customers;
    }
    
    @Override
    public String toString() {
        return "info.dsf.data.model.Useraccess[ userAccessId=" + userAccessId + " ]";
    }
    
}
