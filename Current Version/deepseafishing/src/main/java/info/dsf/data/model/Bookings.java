package info.dsf.data.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

/**
 * An generated entity class for the booking table.
 */
@Entity
@Table(name = "bookings")
public class Bookings implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "booking_id")
    private Integer bookingId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "numberOfPassengers")
    private int numberOfPassengers;
    
    @Column(name = "date_booked")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateBooked;
    
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cost")
    private Double cost;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "registrable")
    private boolean registrable = false;
    
    @JoinColumn(name = "customer_id", referencedColumnName = "customer_id")
    @ManyToOne(optional = false)
    private Customers customerId;

    public Bookings() {
    }

    public Bookings(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public Bookings(Integer bookingId, short numberOfPassengers, boolean registrable) {
        this.bookingId = bookingId;
        this.numberOfPassengers = numberOfPassengers;
        this.registrable = registrable;
    }

    public Integer getBookingId() {
        return bookingId;
    }

    public void setBookingId(Integer bookingId) {
        this.bookingId = bookingId;
    }

    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public void setNumberOfPassengers(int numberOfPassengers) {
        this.numberOfPassengers = numberOfPassengers;
    }

    public Date getDateBooked() {
        return dateBooked;
    }

    public void setDateBooked(Date dateBooked) {
        this.dateBooked = dateBooked;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public boolean getRegistrable() {
        return registrable;
    }

    public void setRegistrable(boolean registrable) {
        this.registrable = registrable;
    }

    public Customers getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customers customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "info.dsf.data.model.Bookings[ bookingId=" + bookingId + " ]";
    }
    
}
