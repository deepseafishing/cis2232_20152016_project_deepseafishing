package info.dsf.service;

import info.dsf.dao.SeaDAO;
import info.dsf.data.jparepository.BookingsRepository;
import info.dsf.data.jparepository.CatchLogRepository;
import info.dsf.data.jparepository.CustomersRepository;
import info.dsf.data.jparepository.UserAccessRepository;
import info.dsf.data.model.Bookings;
import info.dsf.data.model.Catchlog;
import info.dsf.data.model.Customers;
import info.dsf.data.model.Useraccess;
import java.util.ArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Purpose: Implementation class for Court Service which forces all need methods
 * to be create for a successful deployment of application. Also is used as a
 * facade for all controllers;
 */
@Service
public class SeaServiceImpl implements SeaService {
    
    private final UserAccessRepository userRepository;
    private final CustomersRepository customerRepository;
    private final BookingsRepository bookingRepository;
    private final CatchLogRepository catchlogRepository;
    
    @Autowired
    public SeaServiceImpl(UserAccessRepository userRepository, CustomersRepository customerRepository, BookingsRepository bookingRepository, CatchLogRepository catchLogRepository) {
        this.userRepository = userRepository;
        this.customerRepository = customerRepository;
        this.bookingRepository = bookingRepository;
        this.catchlogRepository = catchLogRepository;
    }
    
    @Override
    public String loginMember(String url) {
        System.out.println("RETURNING: Login Confrimation"
                + "FROM: SeaServiceImpl");
        return SeaDAO.getResponseFromRest(url);
    }
    
    @Override
    public ArrayList<Useraccess> getUserByUsername(String username) {
        System.out.println("RETURNING: User By Username"
                + "FROM: SeaServiceImpl");
        return userRepository.findByUsername(username);
    }
    
    @Override
    public boolean saveUser(Useraccess newUser) {
        System.out.println("ADDING: User To Database."
                + "FROM: SeaServiceImpl");
        boolean dataSaved;
        try {
            userRepository.save(newUser);
            dataSaved = true;
        } catch (Exception ex) {
            dataSaved = false;
            System.out.println("HIBERNATE SQL INSERT ERROR: " + ex.toString()
                    + "\nWHERE: " + ex.getClass());
        }
        return dataSaved;
    }
    
    @Override
    public String saveCustomer(Customers newCustomer) {
        System.out.println("ADDING: Customer To Database."
                + "FROM: SeaServiceImpl");
        String message;
        try {
            customerRepository.save(newCustomer);
            message = "Customer was added.";
        } catch (Exception ex) {
            message = "Their was an issue adding customer to database.";
            System.out.println("HIBERNATE SQL INSERT ERROR: " + ex.toString()
                    + "\nWHERE: " + ex.getClass());
        }
        return message;
    }
    
    @Override
    public ArrayList<Customers> getCustomers() {
        System.out.println("RETURNING: All Customers"
                + "FROM: SeaServiceImpl");
        return customerRepository.findAll();
    }
    
    @Override
    public ArrayList<Customers> getCustomerByEmail(String email) {
        System.out.println("RETURNING: Customer By Username"
                + "FROM: SeaServiceImpl");
        return customerRepository.findByEmail(email);
    }
    
    @Override
    public String saveBooking(Bookings newBooking) {
        System.out.println("ADDING: Customer To Database."
                + "FROM: SeaServiceImpl");
        String message;
        try {
            bookingRepository.save(newBooking);
            message = "Booking was added.";
        } catch (Exception ex) {
            message = "Their was an issue adding booking to database.";
            System.out.println("HIBERNATE SQL INSERT ERROR: " + ex.toString()
                    + "\nWHERE: " + ex.getClass());
        }
        return message;
    }
    
    @Override
    public ArrayList<Bookings> getBookings() {
        return bookingRepository.findAll();
    }
    
    @Override
    public ArrayList<Bookings> getBookingByBookingId(int bookingId) {
        return bookingRepository.findByBookingId(bookingId);
    }
    
    @Override
    public String saveCatch(Catchlog newCatch){
        System.out.println("ADDING: Catches To Database."
                + "FROM: SeaServiceImpl");
        String message;
        try {
            catchlogRepository.save(newCatch);
            message = "Catch was added.";
        } catch (Exception ex) {
            message = "Their was an issue adding catches to database.";
            System.out.println("HIBERNATE SQL INSERT ERROR: " + ex.toString()
                    + "\nWHERE: " + ex.getClass());
        }
        return message;
    }
    
    @Override
    public ArrayList<Catchlog> getCatchLogs() {
        System.out.println("RETURNING: Catches from Database"
                + "FROM: SeaServiceImpl");
        return catchlogRepository.findAll();
    }
}
