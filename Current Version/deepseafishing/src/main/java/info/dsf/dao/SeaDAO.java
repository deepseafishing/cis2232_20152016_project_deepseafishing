package info.dsf.dao;

import info.dsf.dao.util.ConnectionUtils;
import info.dsf.dao.util.DbUtils;
import info.dsf.data.model.DatabaseConnection;
import info.dsf.data.model.FinancialReport;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Purpose: Allows you to access methods which require database or web access.
 */
public class SeaDAO {

    /**
     * Gets users privilege id by verifying their login credentials with the
     * CISADMIN application.
     *
     * @param urlString
     * @return
     */
    public static synchronized String getResponseFromRest(String urlString) {
        try {
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                System.out.println("***Failure to Load!***"
                        + "\nHTTP ERROR CODE: " + conn.getResponseCode()
                        + "\nMESSAGE: " + conn.getResponseMessage());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            String accessID = null;
            String output;
            while ((output = br.readLine()) != null) {
                System.out.println("***Useraccess Data Found: " + output + "***");
                accessID = output.replaceAll("\\s+", "");
            }

            if (accessID == null) {
                accessID = "0";
                System.out.println("***Data is null!***");
            }

            conn.disconnect();
            return accessID;
        } catch (MalformedURLException e) {
            System.out.println("***Error with URL!***"
                    + "\nMESSAGE: " + e.getLocalizedMessage());
        } catch (IOException e) {
            System.out.println("***Error with Service!***"
                    + "\nMESSAGE: " + e.getLocalizedMessage());
        }
        return "0";
    }

    /**
     * Database Connection method for report information.
     *
     * @param timePeriod
     * @return List of CourtRoom's
     */
    public static synchronized ArrayList<FinancialReport> getReports(String timePeriod) {
        ArrayList<FinancialReport> report = new ArrayList<>();

        PreparedStatement ps = null;
        String sql;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(new DatabaseConnection("bjmac_deepseafishing", "bjmac", "de234ef")); //set connection

            //get Financial Report information
            switch (timePeriod.toLowerCase()) {
                case "daily":
                    sql = "SELECT SUM(cost) AS sales, SUM(numberOfPassengers) AS totalPassengers FROM bookings WHERE DATE_FORMAT(NOW(),'%Y-%m-%d') = DATE_FORMAT(date_booked,'%Y-%m-%d')";
                    break;
                case "weekly":
                    sql = "SELECT SUM(cost) AS sales, SUM(numberOfPassengers) AS totalPassengers FROM bookings WHERE date_booked BETWEEN DATE_SUB(NOW(), INTERVAL 1 WEEK) and NOW()";
                    break;
                case "monthly":
                    sql = "SELECT SUM(cost) AS sales, SUM(numberOfPassengers) AS totalPassengers FROM bookings WHERE DATE_FORMAT(NOW(),'%Y-%m') = DATE_FORMAT(date_booked,'%Y-%m')";
                    break;
                case "yearly":
                    sql = "SELECT SUM(cost) AS sales, SUM(numberOfPassengers) AS totalPassengers FROM bookings WHERE DATE_FORMAT(NOW(),'%Y') = DATE_FORMAT(date_booked,'%Y')";
                    break;
                default:
                    sql = "";
                    break;
            }

            ps = conn.prepareStatement(sql); //run sql statement
            ResultSet rs = ps.executeQuery(); //get statment results
            while (rs.next()) {
                double sales = rs.getDouble("sales");
                int totalPassengers = rs.getInt("totalPassengers");
                FinancialReport newReport = new FinancialReport(timePeriod, sales, totalPassengers);
                report.add(newReport);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            System.out.println("ERROR GETTING FINANCIAL DATA: " + errorMessage);
        } finally {
            DbUtils.close(ps, conn);
        }
        return report;
    }
}
