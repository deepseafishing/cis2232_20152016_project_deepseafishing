package info.dsf.utils;

import info.dsf.dao.SeaDAO;
import info.dsf.data.model.DatabaseConnection;
import info.dsf.data.model.FinancialReport;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SeaUtil {

    /**
     * Creates a Date Data Type with Current Date
     *
     * @return
     */
    public static Date setDate() {
        Date createdDateTime;
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        createdDateTime = new Date();
        String currentDate = dateFormat.format(createdDateTime);
        try {
            createdDateTime = dateFormat.parse(currentDate);
        } catch (ParseException ex) {
            System.out.println("*** ERROR SETTING DATE ***"
                    + "\nREASON: " + ex.toString());
        }
        return createdDateTime;
    }

    /**
     * Formats Date Input For Database
     *
     * @param datetime
     * @return
     */
    public static Date convertStringDate(String datetime) {
        DateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        DateFormat output = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date parse;
        Date date = null;
        try {
            parse = input.parse(datetime);
            date = output.parse(output.format(parse));
        } catch (ParseException ex) {
            System.out.println("*** ERROR SETTING DATE ***"
                    + "\nREASON: " + ex.toString());
        }
        return date;
    }

    /**
     * Formats Date Input For Database
     *
     * @param date
     * @return
     */
    public static Date convertString(String date) {
        DateFormat input = new SimpleDateFormat("yyyy-MM-dd");
        DateFormat output = new SimpleDateFormat("yyyy-MM-dd");
        Date parse;
        Date newDate = null;
        try {
            parse = input.parse(date);
            newDate = output.parse(output.format(parse));
        } catch (ParseException ex) {
            System.out.println("*** ERROR SETTING DATE ***"
                    + "\nREASON: " + ex.toString());
        }
        return newDate;
    }

    /**
     * Calculates cost for a booking.
     *
     * @param numberOfPeople
     * @return
     */
    public static double calculateCost(int numberOfPeople) {
        double costPerPerson = 40;
        double cost = (costPerPerson * numberOfPeople) + ((costPerPerson * numberOfPeople) * 0.14);
        return cost;
    }

    /**
     * Gets all reports.
     *
     * @return
     */
    public static ArrayList<FinancialReport> getReports() {
        ArrayList<FinancialReport> allReports = new ArrayList<>();
        String[] timePeriods = {"Daily", "Weekly", "Monthly", "Yearly"};
        for (String periods : timePeriods) {
            ArrayList<FinancialReport> report = SeaDAO.getReports(periods);
            if (!report.isEmpty()) {
                allReports.add(report.get(0));
            }
        }
        return allReports;
    }

    /**
     * BJs Hashing Method
     *
     * @param inPassword
     * @return
     */
    public static String getHashPassword(String inPassword) {
        try {
            return getMD5Hash(inPassword);
        } catch (Exception ex) {

        }
        return "Should not see this!";
    }

    public static String getMD5Hash(String passwordIn) throws Exception {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.update(passwordIn.getBytes(), 0, passwordIn.length());
        return "" + new BigInteger(1, m.digest()).toString(16);
    }
}
