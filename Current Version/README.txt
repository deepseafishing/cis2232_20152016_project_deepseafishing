PLEASE READ THE MAIN PROJECTS README ON HOW TO SETUP! (ALWAYS UPLOAD/UPDATE DATABASE BEFORE USING IF REQUIRED!)

Quick Overview:

	-Application was built to allow users to use the deepseafishing application for:
		-Booking Events
		-Registiering Customers
		-Generating Financial Reports
	
The deepseafishing application has the following updates:

	-DATABASE Tables:
		-bookings
		-customers (also used for staff)
		-useraccess (by default all users are customers)
		-codetypes and codevalues tables
	-Functionality:
		-Login using CISADMIN rest service
			-DEFAULT LOGIN:
				-USERNAME: staff@deepseafishing.com
				-PASSWORD: 123
	
Deep Sea Fishing: Rest Services Added:

	-To Access When Uploaded To Server:
		-http://bjmac.hccis.info:8080/deepseafishing/rest/dataaccess/
	-These are all the avaible service names (PLEASE ENTER ALL DATA IN ORDER SHOWN):
		-signup/creditCard,lastName,firstName,phone,email
		-addbooking/creditCard,lastName,firstName,phone,email,date,time,numberOfPassengers
		-bookings
			-returns a json string.
		-customers
			-returns a json string.
		-financialreport
			-returns a json string.
		-NOT YET UPLOADED
			-addCatch/dateOfTrip,timeOfTrip,notes,mackerelCount,codCount,sculpinCount,halibutCount,lobsterCount
			-catchlogs
				-returns a json string.

Deep Sea Fishing: Rest Services UPDATE:
	
	-All service will now use at least two of these HTTP Repsonse Codes:
		-200: Data Found
		-201: Data Added
		-204: Data Not Found
		-400: Issue with inputed data or Issue related to client
		-500: Internal Server Issue
	-Please review the DeepSeaFishingRest.java file to see what response codes are being returned.
			
Deep Sea Fishing: Data Integrity:

	-LOGIN:
		-by default the customers email (username) and phone number (password) will be their login information
	-SERVICE: addbooking
		-customer doesn't need to exist in system auto creates user if they don't exist
		-cost is calculated using method in application ($40.00 per person plus tax)
		-numberOfPassengers must be 1 to 18
		-date must be YYYY-MM-DD or 2016-01-31
		-time must be 00:00 to 23:59 (24 hour time)
		
Deep Sea Fishing: Rest Services Added:

	-To Access User Login (No Hashing Nesscary)
		-http://bjmac.hccis.info:8080/cisadmin/rest/useraccess/databaseName,username,password
			-returns 1 for staff or 2 for customer
	
Warnings/Issues:

	-On Server Verison
		-Located In Current Directory
		-Issue with Login service not accepting customers logins (returns 0 and not 2).
	-On Localhost Verison
		-Located In Archive
		-None Found (But Possible)