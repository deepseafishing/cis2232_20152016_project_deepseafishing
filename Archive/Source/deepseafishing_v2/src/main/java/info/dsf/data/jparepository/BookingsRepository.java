package info.dsf.data.jparepository;

import info.dsf.data.model.Bookings;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Purpose: To allow JPA functionality with the bookings table.
 */
@Repository
public interface BookingsRepository  extends CrudRepository<Bookings, Integer> {
    
    @Override
    public ArrayList<Bookings> findAll();
    
    public ArrayList<Bookings> findByBookingId(int bookingId);
}
