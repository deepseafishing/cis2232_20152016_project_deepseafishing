package info.dsf.web;

import info.dsf.data.model.Useraccess;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Purpose: Directs server to correct page and adds needed information to each
 * page.
 */
@Controller
public class OtherController {

    @RequestMapping("/other/error")
    public String showError(Model model, HttpSession session) {
        model.addAttribute("userToken", session.getAttribute("userToken"));
        return "other/error";
    }

    @RequestMapping("/other/unauthorizedaccess")
    public String showUnauthorizedAccess(Model model, HttpSession session) {
        model.addAttribute("userToken", session.getAttribute("userToken"));
        return "other/unauthorizedaccess";
    }

    @RequestMapping("/other/help")
    public String showHelp(Model model, HttpSession session) {
        model.addAttribute("userToken", session.getAttribute("userToken"));
        return "other/help";
    }

    @RequestMapping("/other/about")
    public String showAbout(Model model, HttpSession session) {
        model.addAttribute("userToken", session.getAttribute("userToken"));
        return "other/about";

    }

    @RequestMapping("/PDFViewer")
    public String showPDFViewer(Model model, HttpSession session) {
        model.addAttribute("userToken", session.getAttribute("userToken"));
        return "other/PDFViewer";
    }

    @RequestMapping("/other/welcome")
    public String showLogin(Model model, HttpSession session, @ModelAttribute("login") Useraccess login) {
        return "other/welcome";
    }

    @RequestMapping("/")
    public String showHome(Model model, HttpSession session, @ModelAttribute("login") Useraccess login) {
        System.out.println("IN: Controller of root.");
        return "other/welcome";
    }
}
