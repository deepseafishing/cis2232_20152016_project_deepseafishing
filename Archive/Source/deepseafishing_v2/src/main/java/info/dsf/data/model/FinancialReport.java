package info.dsf.data.model;

public class FinancialReport {

    private String timePeriod;
    private double sales;
    private int totalPassengers;

    public FinancialReport(String timePeriod, double sales, int totalPassengers) {
        this.timePeriod = timePeriod;
        this.sales = sales;
        this.totalPassengers = totalPassengers;
    }

    public FinancialReport() {
    }

    public String getTimePeriod() {
        return timePeriod;
    }

    public void setTimePeriod(String timePeriod) {
        this.timePeriod = timePeriod;
    }

    public double getSales() {
        return sales;
    }

    public void setSales(double sales) {
        this.sales = sales;
    }

    public int getTotalPassengers() {
        return totalPassengers;
    }

    public void setTotalPassengers(int totalPassengers) {
        this.totalPassengers = totalPassengers;
    }

}
