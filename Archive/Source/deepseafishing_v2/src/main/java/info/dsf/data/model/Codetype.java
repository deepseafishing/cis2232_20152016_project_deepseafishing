package info.dsf.data.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * An generated entity class for the code type table.
 */
@Entity
@Table(name = "CodeType")
public class Codetype implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "CodeTypeId")
    private Integer codeTypeId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "englishDescription")
    private String englishDescription;
    
    @Size(max = 100)
    @Column(name = "frenchDescription")
    private String frenchDescription;
    
    @Column(name = "createdDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDateTime;
    
    @Size(max = 20)
    @Column(name = "createdUserId")
    private String createdUserId;
    
    @Column(name = "updatedDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDateTime;
    
    @Size(max = 20)
    @Column(name = "updatedUserId")
    private String updatedUserId;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "codeTypeId")
    private Collection<Codevalue> codevalueCollection;

    public Codetype() {
    }

    public Codetype(Integer codeTypeId) {
        this.codeTypeId = codeTypeId;
    }

    public Codetype(Integer codeTypeId, String englishDescription) {
        this.codeTypeId = codeTypeId;
        this.englishDescription = englishDescription;
    }

    public Integer getCodeTypeId() {
        return codeTypeId;
    }

    public void setCodeTypeId(Integer codeTypeId) {
        this.codeTypeId = codeTypeId;
    }

    public String getEnglishDescription() {
        return englishDescription;
    }

    public void setEnglishDescription(String englishDescription) {
        this.englishDescription = englishDescription;
    }

    public String getFrenchDescription() {
        return frenchDescription;
    }

    public void setFrenchDescription(String frenchDescription) {
        this.frenchDescription = frenchDescription;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Date getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(Date updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    public String getUpdatedUserId() {
        return updatedUserId;
    }

    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    public Collection<Codevalue> getCodevalueCollection() {
        return codevalueCollection;
    }

    public void setCodevalueCollection(Collection<Codevalue> codevalueCollection) {
        this.codevalueCollection = codevalueCollection;
    }

    @Override
    public String toString() {
        return "info.dsf.data.model.Codetype[ codeTypeId=" + codeTypeId + " ]";
    }
    
}
