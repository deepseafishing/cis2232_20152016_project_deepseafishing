package info.dsf.data.jparepository;

import info.dsf.data.model.Useraccess;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Purpose: To allow JPA functionality with the User Access table.
 */
@Repository
public interface UserAccessRepository extends CrudRepository<Useraccess, Integer> {
    
    public ArrayList<Useraccess> findByUsername(String username);
}