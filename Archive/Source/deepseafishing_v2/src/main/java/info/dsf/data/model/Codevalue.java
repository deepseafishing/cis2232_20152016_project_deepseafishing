package info.dsf.data.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * An generated entity class for the code value table.
 */
@Entity
@Table(name = "CodeValue")
public class Codevalue implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "codeValueSequence")
    private Integer codeValueSequence;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "englishDescription")
    private String englishDescription;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "englishDescriptionShort")
    private String englishDescriptionShort;
    
    @Size(max = 100)
    @Column(name = "frenchDescription")
    private String frenchDescription;
    
    @Size(max = 20)
    @Column(name = "frenchDescriptionShort")
    private String frenchDescriptionShort;
    
    @Column(name = "createdDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdDateTime;
    
    @Size(max = 20)
    @Column(name = "createdUserId")
    private String createdUserId;
    
    @Column(name = "updatedDateTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedDateTime;
    
    @Size(max = 20)
    @Column(name = "updatedUserId")
    private String updatedUserId;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userTypeCode")
    private Collection<Useraccess> useraccessCollection;
    
    @JoinColumn(name = "codeTypeId", referencedColumnName = "CodeTypeId")
    @ManyToOne(optional = false)
    private Codetype codeTypeId;

    public Codevalue() {
    }

    public Codevalue(Integer codeValueSequence) {
        this.codeValueSequence = codeValueSequence;
    }

    public Codevalue(Integer codeValueSequence, String englishDescription, String englishDescriptionShort) {
        this.codeValueSequence = codeValueSequence;
        this.englishDescription = englishDescription;
        this.englishDescriptionShort = englishDescriptionShort;
    }

    public Integer getCodeValueSequence() {
        return codeValueSequence;
    }

    public void setCodeValueSequence(Integer codeValueSequence) {
        this.codeValueSequence = codeValueSequence;
    }

    public String getEnglishDescription() {
        return englishDescription;
    }

    public void setEnglishDescription(String englishDescription) {
        this.englishDescription = englishDescription;
    }

    public String getEnglishDescriptionShort() {
        return englishDescriptionShort;
    }

    public void setEnglishDescriptionShort(String englishDescriptionShort) {
        this.englishDescriptionShort = englishDescriptionShort;
    }

    public String getFrenchDescription() {
        return frenchDescription;
    }

    public void setFrenchDescription(String frenchDescription) {
        this.frenchDescription = frenchDescription;
    }

    public String getFrenchDescriptionShort() {
        return frenchDescriptionShort;
    }

    public void setFrenchDescriptionShort(String frenchDescriptionShort) {
        this.frenchDescriptionShort = frenchDescriptionShort;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public String getCreatedUserId() {
        return createdUserId;
    }

    public void setCreatedUserId(String createdUserId) {
        this.createdUserId = createdUserId;
    }

    public Date getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(Date updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

    public String getUpdatedUserId() {
        return updatedUserId;
    }

    public void setUpdatedUserId(String updatedUserId) {
        this.updatedUserId = updatedUserId;
    }

    public Collection<Useraccess> getUseraccessCollection() {
        return useraccessCollection;
    }

    public void setUseraccessCollection(Collection<Useraccess> useraccessCollection) {
        this.useraccessCollection = useraccessCollection;
    }

    public Codetype getCodeTypeId() {
        return codeTypeId;
    }

    public void setCodeTypeId(Codetype codeTypeId) {
        this.codeTypeId = codeTypeId;
    }

    @Override
    public String toString() {
        return "info.dsf.data.model.Codevalue[ codeValueSequence=" + codeValueSequence + " ]";
    }
    
}
