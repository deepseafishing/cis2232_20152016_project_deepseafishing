/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.service;

import info.hccis.admin.model.DatabaseConnection;

/**
*
* @author Stephane Levesque
* @since December 19, 2015
*/
public interface FishingService {
    
    public abstract Double getFinanceValue(DatabaseConnection connection, int part);
    
    public abstract Double getCustomerValue(DatabaseConnection connection, int part);
    
}
