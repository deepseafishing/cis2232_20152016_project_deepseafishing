/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.service;

import info.hccis.admin.dao.FishingDAO;
import info.hccis.admin.model.DatabaseConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
*
* @author Stephane Levesque
* @since December 19, 2015
*/
@Service
public class FishingServiceImp implements FishingService{
    
    private final FishingDAO fishingDAO;
    
    /**
    *
    * @author Stephane Levesque
    * @since December 19, 2015
    */
    @Autowired
    public FishingServiceImp(FishingDAO fishingDAO){
            this.fishingDAO = fishingDAO;
    }

    /**
    *
    * @author Stephane Levesque
    * @since December 19, 2015
    */
    @Override
    public Double getFinanceValue(DatabaseConnection connection, int part) {
        if(part == 0)
    	{
    		return fishingDAO.fetchDailyFinance(connection);
    	}
    	else if(part == 1)
    	{
    		return fishingDAO.fetchWeeklyFinance(connection);
    	}
    	else if(part == 2)
    	{
    		return fishingDAO.fetchMonthlyFinance(connection);
    	}
    	else
    	{
    		return fishingDAO.fetchYearlyFinance(connection);
    	}
       
    }

    /**
    *
    * @author Stephane Levesque
    * @since December 19, 2015
    */
    @Override
    public Double getCustomerValue(DatabaseConnection connection, int part) {
        if(part == 0)
    	{
    		return fishingDAO.fetchDailyCustomer(connection);
    	}
    	else if(part == 1)
    	{
    		return fishingDAO.fetchWeeklyCustomer(connection);
    	}
    	else if(part == 2)
    	{
    		return fishingDAO.fetchMonthlyCustomer(connection);
    	}
    	else
    	{
    		return fishingDAO.fetchYearlyCustomer(connection);
    	}
    }
    
}
