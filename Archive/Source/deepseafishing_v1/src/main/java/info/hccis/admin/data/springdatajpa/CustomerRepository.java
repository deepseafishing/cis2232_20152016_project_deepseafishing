/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.Bookings;
import info.hccis.admin.model.jpa.Customers;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author takee_000
 */
@Repository
public interface CustomerRepository extends CrudRepository<Customers, Integer> {
    @Override
    public ArrayList<Customers> findAll();
}
