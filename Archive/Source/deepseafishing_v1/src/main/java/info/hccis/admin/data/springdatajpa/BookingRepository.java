/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.data.springdatajpa;

import info.hccis.admin.model.jpa.Bookings;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author takee_000
 */
@Repository
public interface BookingRepository extends CrudRepository<Bookings, Integer>{
    @Override
    public ArrayList<Bookings> findAll();
    
//    public ArrayList<Bookings> findAllByDate();
}
