/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.dao;

import info.hccis.admin.dao.util.ConnectionUtils;
import info.hccis.admin.dao.util.DbUtils;
import info.hccis.admin.model.DatabaseConnection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Stephane Levesque
 * @since December 19, 2015
 */
@Repository
public class FishingDAO {
    
    /**
    *
    * @author Stephane Levesque
    * @since December 19, 2015
    */
    public Double fetchDailyFinance(DatabaseConnection databaseConnection) {
        Double codes = 0.00D;

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);
            
            sql = "SELECT sum(cost) FROM `bookings` where DATE_FORMAT(NOW(),'%Y-%m-%d') = DATE_FORMAT(date,'%Y-%m-%d') ";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                codes += rs.getDouble(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return codes;

    }
	
        /**
        *
        * @author Stephane Levesque
        * @since December 19, 2015
        */
	public Double fetchWeeklyFinance(DatabaseConnection databaseConnection) {
        Double codes = 0.00D;

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);
            
            sql = "SELECT sum(cost) FROM `bookings` where date between date_sub(now(),INTERVAL 1 WEEK) and now() ";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                codes += rs.getDouble(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return codes;

    }
	/**
        *
        * @author Stephane Levesque
        * @since December 19, 2015
        */
	public Double fetchMonthlyFinance(DatabaseConnection databaseConnection) {
        Double codes = 0.00D;

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);
            
            sql = "SELECT sum(cost) FROM `bookings` where DATE_FORMAT(NOW(),'%Y-%m') = DATE_FORMAT(date,'%Y-%m') ";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                codes += rs.getDouble(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return codes;

    }
	
        /**
        *
        * @author Stephane Levesque
        * @since December 19, 2015
        */
	public Double fetchYearlyFinance(DatabaseConnection databaseConnection) {
        Double codes = 0.00D;

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);
            
            sql = "SELECT sum(cost) FROM `bookings` where DATE_FORMAT(NOW(),'%Y') = DATE_FORMAT(date,'%Y') ";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                codes += rs.getDouble(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return codes;

    }
	
        /**
        *
        * @author Stephane Levesque
        * @since December 19, 2015
        */
	public Double fetchDailyCustomer(DatabaseConnection databaseConnection) {
        Double codes = 0.00D;

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);
            
            sql = "SELECT sum(numberOfPassengers) FROM `bookings` where DATE_FORMAT(NOW(),'%Y-%m-%d') = DATE_FORMAT(date,'%Y-%m-%d') ";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                codes += rs.getDouble(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return codes;

    }
	
        /**
        *
        * @author Stephane Levesque
        * @since December 19, 2015
        */
	public Double fetchWeeklyCustomer(DatabaseConnection databaseConnection) {
        Double codes = 0.00D;

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);
            
            sql = "SELECT sum(numberOfPassengers) FROM `bookings` where date between date_sub(now(),INTERVAL 1 WEEK) and now() ";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                codes += rs.getDouble(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return codes;

    }
	
        /**
        *
        * @author Stephane Levesque
        * @since December 19, 2015
        */
	public Double fetchMonthlyCustomer(DatabaseConnection databaseConnection) {
        Double codes = 0.00D;

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);
            
            sql = "SELECT sum(numberOfPassengers) FROM `bookings` where DATE_FORMAT(NOW(),'%Y-%m') = DATE_FORMAT(date,'%Y-%m') ";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                codes += rs.getDouble(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return codes;

    }
	
        /**
        *
        * @author Stephane Levesque
        * @since December 19, 2015
        */
	public Double fetchYearlyCustomer(DatabaseConnection databaseConnection) {
        Double codes = 0.00D;

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);
            
            sql = "SELECT sum(numberOfPassengers) FROM `bookings` where DATE_FORMAT(NOW(),'%Y') = DATE_FORMAT(date,'%Y') ";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                codes += rs.getDouble(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return codes;

    }
    
}
