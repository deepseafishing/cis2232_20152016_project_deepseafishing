package info.hccis.admin.web;

import info.hccis.admin.data.springdatajpa.BookingRepository;
import info.hccis.admin.data.springdatajpa.CustomerRepository;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.jpa.Bookings;
import info.hccis.admin.model.jpa.Customers;
import info.hccis.admin.model.jpa.Users;
import info.hccis.admin.service.CodeService;
import info.hccis.admin.service.FishingService;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
*
* @author Stephane Levesque
* @since December 19, 2015
* 
* This class serves as the main Controller for all fishing related functions and reports
*/
@Controller
@RequestMapping("/fishing")
public class FishingController {
    FishingService fishingService;
    BookingRepository br;
    CustomerRepository cr;

    /**
    *
    * @author Stephane Levesque
    * @since December 19, 2015
    */
    @Autowired
    public FishingController(FishingService fishingService, BookingRepository br, CustomerRepository cr) {
        this.fishingService = fishingService;
        this.br = br;
        this.cr = cr;
    }
    
    /**
    *
    * @author Stephane Levesque
    * @since December 19, 2015
    */
    @InitBinder
    public void setAllowedFields(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }
    
    /**
    *
    * @author Stephane Levesque
    * @since December 19, 2015
    */
    @RequestMapping("financeReport")
    public String financeReport(Model model, HttpSession session, DatabaseConnection dbConnectionIn) {
    	 model.addAttribute("db", dbConnectionIn);
         session.setAttribute("db", dbConnectionIn);
         DecimalFormat df2 = new DecimalFormat( "#,###,###,##0.00" );
         Double daily = fishingService.getFinanceValue(dbConnectionIn, 0);
         Double weekly = fishingService.getFinanceValue(dbConnectionIn, 1);
         Double monthly = fishingService.getFinanceValue(dbConnectionIn, 2);
         Double yearly = fishingService.getFinanceValue(dbConnectionIn, 3);
         model.addAttribute("daily", df2.format(daily));
         model.addAttribute("weekly", df2.format(weekly));
         model.addAttribute("monthly", df2.format(monthly));
         model.addAttribute("yearly", df2.format(yearly));
         return "/fishing/financeReport";
    }
    
    /**
    *
    * @author Stephane Levesque
    * @since December 19, 2015
    */
    @RequestMapping("volumeReport")
    public String volumeReport(Model model, HttpSession session, DatabaseConnection dbConnectionIn) {
    	 model.addAttribute("db", dbConnectionIn);
         session.setAttribute("db", dbConnectionIn);
         DecimalFormat df2 = new DecimalFormat( "#,###,###,##0.00" );
          Double daily = fishingService.getCustomerValue(dbConnectionIn, 0);
         Double weekly = fishingService.getCustomerValue(dbConnectionIn, 1);
         Double monthly = fishingService.getCustomerValue(dbConnectionIn, 2);
         Double yearly = fishingService.getCustomerValue(dbConnectionIn, 3);
         model.addAttribute("daily", df2.format(daily));
         model.addAttribute("weekly", df2.format(weekly));
         model.addAttribute("monthly", df2.format(monthly));
         model.addAttribute("yearly", df2.format(yearly));
         return "/fishing/volumeReport";
    }
    
    /**
     * @author Ben Bain
     * @edited December 20, 2015
     * @param model
     * @param session
     * @return 
     */
    @RequestMapping("userAccess")
    public String showMessage(Model model, HttpSession session){
        Users user = new Users();
        model.addAttribute("user", user);
        return "/fishing/userAccess";
    }
    
    /**
     * @author Ben Bain
     * @edited December 20, 2015
     * @param model
     * @param session
     * @return 
     */
    @RequestMapping("adminLogin")
    public String login(Model model, HttpSession session){
        Users user = new Users();
        model.addAttribute("user", user);
        session.setAttribute("adminLoggedIn", true);
        return "/fishing/index";
    }
    
    /**
     * @author Ben Bain 
     * @edited December 20, 2015
     * @param model
     * @param session
     * @return 
     */
    @RequestMapping("index")
    public String showBookings(Model model, HttpSession session){
        //todo logic for showing bookings goes here
        ArrayList<Bookings> booking = br.findAll();
        model.addAttribute("booking", booking);
        return "/fishing/index";
    }
    /**
     * @author Brodie Macbeath
     * @param model
     * @param session
     * @return 
     */
    @RequestMapping("newCustomer")
    public String newCustomer(Model model, HttpSession session){
        Customers newCustomer = new Customers();
        model.addAttribute("customer", newCustomer);
        return "/fishing/newCustomer";
    }
    /**
     * @author Brodie Macbeath
     * @since December 17, 2015
     * @edited December 19, 2015
     * @param model
     * @param session
     * @param customer
     * @return 
     */
    @RequestMapping("addCustomer")
    public String addCustomer(Model model, HttpSession session, Customers customer){
        cr.save(customer);
        return "/fishing/index";
    }
    
    /**
     * @author Ben Bain
     * @edited December 20, 2015
     * @param model
     * @param session
     * @return 
     */
    @RequestMapping("newBooking")
    public String newBooking(Model model, HttpSession session){
        Bookings booking = new Bookings();
        model.addAttribute("booking", booking);
        return "/fishing/newBooking";
    }
    /**
     * @author Brodie Macbeath
     * @param model
     * @param session
     * @param booking
     * @return 
     */
    @RequestMapping("addBooking")
    public String addBooking(Model model, HttpSession session, Bookings booking){
        model.addAttribute("booking", booking);
        double cost = 0.0;
        cost = booking.calculatePrice();
        booking.setCost(cost);
        
        br.save(booking);
        return "/fishing/index";
    }
}
