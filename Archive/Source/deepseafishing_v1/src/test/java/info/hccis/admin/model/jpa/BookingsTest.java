/**
 * This class holds the test functionality of the Bookings class.
 * In this class we will test the CalculatePrice method to ensure the method is correct.
 */
package info.hccis.admin.model.jpa;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Ben Bain
 * @since December 20, 2015
 * @purpose Method testing for the bookings class.
 */
public class BookingsTest {
    public BookingsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * This method will test the calculate price method in the Bookings class.
     */
    @Test
    public void calculatePrice(){
        Bookings booking = new Bookings();
        booking.setNumberOfPassengers(10);
        booking.calculatePrice();
        Assert.assertEquals(400.0, 400.0, 0.00);
    }
}
