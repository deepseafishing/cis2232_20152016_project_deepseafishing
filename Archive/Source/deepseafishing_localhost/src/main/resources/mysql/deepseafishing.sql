/*Creates the database*/
CREATE DATABASE deepseafishing;

/*use the new database*/
USE deepseafishing;

--
-- Database tables from Codes Database for CISAdmin
--

/*Table structure for table CodeType*/
CREATE TABLE IF NOT EXISTS `CodeType` (
  `codeTypeId` int(3) NOT NULL PRIMARY KEY COMMENT 'This is the primary key for code types',
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COMMENT='This hold the code types that are available for the applicat';

/*Dumping data for table CodeType*/
INSERT INTO `codeType` (`codeTypeId`, `englishDescription`, `frenchDescription`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 'Member Privileges', '', '2015-11-12 15:15:59', 'Admin', '2015-11-12 15:15:59', 'Admin');

/*AUTO_INCREMENT for table CodeType*/
ALTER TABLE `CodeType` MODIFY `CodeTypeId` int(3) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key for code types', AUTO_INCREMENT = 2;

/*Table structure for table CodeValue*/
CREATE TABLE IF NOT EXISTS `CodeValue` (
  `codeTypeId` int(3) NOT NULL COMMENT 'see code_type table',
  `codeValueSequence` int(3) NOT NULL PRIMARY KEY,
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `englishDescriptionShort` varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `frenchDescriptionShort` varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL,
  FOREIGN KEY (codeTypeId) REFERENCES CodeType(codeTypeId)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='This will hold code values for the application.';

/*Dumping data for table CodeValue*/
INSERT INTO `CodeValue` (`codeTypeId`, `codeValueSequence`, `englishDescription`, `englishDescriptionShort`, `frenchDescription`, `frenchDescriptionShort`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 1, '', 'Admin', '', '', '2015-11-12 15:15:59', 'Admin', '2015-11-12 15:15:59', 'Admin'),
(1, 2, '', 'Customer', '', '', '2015-11-12 15:15:59', 'Admin', '2015-11-12 15:15:59', 'Admin');

/*AUTO_INCREMENT for table CodeValue*/
ALTER TABLE `CodeValue` MODIFY `codeValueSequence` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 3;

/*Table structure for table useraccess*/
CREATE TABLE IF NOT EXISTS `useraccess` (
  `userAccessId` int(3) NOT NULL PRIMARY KEY,
  `username` varchar(100) NOT NULL UNIQUE KEY COMMENT 'Unique user name for app',
  `password` varchar(128) NOT NULL,
  `userTypeCode` int(3) NOT NULL DEFAULT '2' COMMENT 'CodeValue #2 for Customer.',
  `createdDateTime` datetime DEFAULT NULL COMMENT 'When user was created.',
  FOREIGN KEY (userTypeCode) REFERENCES CodeValue(codeValueSequence)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Dumping data for table useraccess*/
INSERT INTO `useraccess` (`userAccessId`, `username`, `password`, `userTypeCode`, `createdDateTime`) 
VALUES (1, 'staff@deepseafishing.com', '9021234567', 1, '2016-01-22 10:00:00');

/*AUTO_INCREMENT for table useraccess*/
ALTER TABLE `useraccess` MODIFY `userAccessId` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 2;

--
-- Database tables created for deepseafishing application
--

/*table for customers*/
CREATE TABLE IF NOT EXISTS `customers` (
  `customer_id` INT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `credit_card` VARCHAR(16) NOT NULL DEFAULT '',
  `last_name` VARCHAR(35) NOT NULL DEFAULT '',
  `first_name` VARCHAR(35) NOT NULL DEFAULT '',
  `phone` VARCHAR(128) NOT NULL DEFAULT '',
  `email` VARCHAR(100) NOT NULL DEFAULT '',
  FOREIGN KEY (customer_id) REFERENCES useraccess(`userAccessId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*add customer*/
INSERT INTO `customers` (`customer_id`, `credit_card`, `last_name`, `first_name`, `phone`, `email`) 
VALUES (1, 'N/A', 'Account', 'Staff', '9021234567', 'staff@deepseafishing.com');

/*update auto increment*/
ALTER TABLE customers MODIFY customer_id INT(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 2;

/*table for customers*/
CREATE TABLE IF NOT EXISTS `bookings` (
  `booking_id` INT(5) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `customer_id` INT(5) NOT NULL DEFAULT '1',
  `numberOfPassengers` SMALLINT(2) NOT NULL DEFAULT '1',
  `date_booked` DATETIME,
  `cost` DOUBLE,
  `registrable` TINYINT(1) NOT NULL DEFAULT '0',
  FOREIGN KEY (customer_id) REFERENCES customers(`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*add customer*/
INSERT INTO `bookings` (`booking_id`, `customer_id`, `date_booked`, `cost`, `registrable`) 
VALUES (1, 1, '2016-01-22 10:00:00', 45.60, 1);

/*update auto increment*/
ALTER TABLE bookings MODIFY booking_id INT(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT = 2;

/*table for catchlog*/
CREATE TABLE IF NOT EXISTS `catchlog` (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    dateOfTrip DATE NOT NULL,
    timeOfTrip VARCHAR(3) NOT NULL,
    mackerelCount INT(3) NOT NULL ,
    codCount INT(3) NOT NULL,
    sculpinCount INT(3) NOT NULL,
    halibutCount INT(3) NOT NULL,
    lobsterCount INT(3) NOT NULL,
    notes VARCHAR(80) NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*create a user in database*/
grant select, insert, update, delete on deepseafishing.*
             to 'admin'@'localhost'
             identified by 'password';
flush privileges;
