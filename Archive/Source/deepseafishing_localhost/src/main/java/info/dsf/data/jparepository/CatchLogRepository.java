package info.dsf.data.jparepository;

import info.dsf.data.model.Catchlog;
import java.util.ArrayList;
import org.springframework.data.repository.CrudRepository;

/**
 *Purpose: To allow JPA functionality with the catch log table.
 */
public interface CatchLogRepository extends CrudRepository<Catchlog, Integer> {
    
    @Override
    public ArrayList<Catchlog> findAll();
}
