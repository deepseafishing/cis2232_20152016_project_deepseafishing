package info.dsf.service;

import info.dsf.data.model.Bookings;
import info.dsf.data.model.Catchlog;
import info.dsf.data.model.Customers;
import info.dsf.data.model.Useraccess;
import java.util.ArrayList;

/**
 * Purpose: Abstract methods for the implementation class.
 */
public interface SeaService {

    /**
     *
     * @param url
     * @return
     */
    public abstract String loginMember(String url);

    /**
     *
     * @param username
     * @return
     */
    public abstract ArrayList<Useraccess> getUserByUsername(String username);

    /**
     *
     * @param newUser
     * @return
     */
    public abstract boolean saveUser(Useraccess newUser);

    /**
     *
     * @param newCustomer
     * @return
     */
    public abstract String saveCustomer(Customers newCustomer);

    /**
     *
     * @return
     */
    public abstract ArrayList<Customers> getCustomers();

    /**
     *
     * @param email
     * @return
     */
    public abstract ArrayList<Customers> getCustomerByEmail(String email);

    /**
     *
     * @param newBooking
     * @return
     */
    public abstract String saveBooking(Bookings newBooking);

    /**
     *
     * @return
     */
    public abstract ArrayList<Bookings> getBookings();

    /**
     *
     * @param bookingId
     * @return
     */
    public abstract ArrayList<Bookings> getBookingByBookingId(int bookingId);

    /**
     *
     * @param newCatch
     * @return
     */
    public abstract String saveCatch(Catchlog newCatch);

    /**
     *
     * @return
     */
    public abstract ArrayList<Catchlog> getCatchLogs();
}
