package info.dsf.web.service;

import info.dsf.data.model.Bookings;
import info.dsf.data.model.Catchlog;
import info.dsf.data.model.Codevalue;
import info.dsf.data.model.Customers;
import info.dsf.data.model.FinancialReport;
import info.dsf.data.model.Useraccess;
import info.dsf.service.SeaService;
import info.dsf.utils.SeaUtil;
import java.util.ArrayList;
import java.util.Date;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * Response Code Keys:
 *
 * 200: Data Found / 201: Data Added / 204: Data Not Found / 400: Issue with
 * inputed data or Issue related to client / 500: Internal Server Issue
 * 
*/
@Component
@Path("/dataaccess")
@Scope("request")
public class DeepSeaFishingRest {

    @Resource
    private final SeaService seaService;

    public DeepSeaFishingRest(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.seaService = applicationContext.getBean(SeaService.class);
    }

    /**
     * Allows account data to be added. User must send data in this order:
     * /creditCard,lastName,firstName,phone,email
     *
     * @param userData
     * @return
     */
    @GET
    @Path("/signup/{param}")
    public Response accountSignup(@PathParam("param") String userData) {
        int responseCode; //set response code
        String message; //set default message
        int customerId = (seaService.getCustomers().size() + 1); //get current index 
        String[] accountData = userData.split(","); //get account data

        if (accountData.length == 5) {
            //create customer object
            Customers customer = setCustomer(accountData[0], accountData[1], accountData[2], accountData[3], accountData[4].toLowerCase());

            //check if user exist and submit to database
            if (seaService.getCustomerByEmail(customer.getEmail().toLowerCase()).isEmpty()) {
                customer.setCustomerId(customerId);
                boolean added = addCustomer(customerId, customer);
                if (added) {
                    message = "Customer was added.";
                    responseCode = 201;
                } else {
                    message = "System Error: Customer can't be added.";
                    responseCode = 500;
                }
            } else {
                message = "Account exist.";
                responseCode = 400;
            }
        } else {
            message = "Account data is missing.";
            responseCode = 400;
        }
        return Response.status(responseCode).entity(message).build();
    }

    /**
     * Allows booking data to be added. User must send data in this order:
     * /creditCard,lastName,firstName,phone,email,date,time,numberOfPassengers
     *
     * Date is set as YYYY-MM-DD or 2016-01-31. Time is set as 24-hour clock
     * which is 00:00 to 23:59.
     *
     * @param userData
     * @return
     */
    @GET
    @Path("/addbooking/{param}")
    public Response addBooking(@PathParam("param") String userData) {
        int responseCode; //set response code
        String message; //set default message
        boolean added; //set boolean for adding customer
        int customerId = (seaService.getCustomers().size() + 1); //get current index 
        String[] accountData = userData.split(","); //get account data

        if (accountData.length == 8) {

            //create customer object
            Customers customer = setCustomer(accountData[0], accountData[1], accountData[2], accountData[3], accountData[4].toLowerCase());

            //check if user exist and submit to database
            if (seaService.getCustomerByEmail(customer.getEmail().toLowerCase()).isEmpty()) {
                customer.setCustomerId(customerId);
                added = addCustomer(customerId, customer);
            } else {
                customer.setCustomerId(seaService.getCustomerByEmail(customer.getEmail().toLowerCase()).get(0).getCustomerId()); //update customer object with customers id
                added = true;
            }

            //get datetime, numberofpassengers for booking
            String datetime = accountData[5] + "T" + accountData[6];
            String dateRegex = "(201[6-9]|202[0-9])-(0[1-9]|1[0-2])-([0-2][0-9]|3[0-2])T(0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])";

            //validate user input
            if (StringUtils.isNumeric(accountData[7]) && datetime.matches(dateRegex) && added) { //make sure number of passengers is a number
                Date dateBooked = SeaUtil.convertStringDate(datetime);
                int numberOfPassengers = Integer.parseInt(accountData[7]); //get number of passengers
                if (numberOfPassengers > 0 && numberOfPassengers < 19) { //make sure correct number of passengers
                    Bookings newBooking = new Bookings();  //create booking object
                    newBooking.setNumberOfPassengers(numberOfPassengers); //set number of passengers
                    newBooking.setDateBooked(dateBooked); //set date booked
                    newBooking.setCost(SeaUtil.calculateCost(numberOfPassengers)); //set cost of booking
                    newBooking.setCustomerId(customer); //set customer for booking   
                    newBooking.setRegistrable(false); //ignore (has a purpose on site)
                    message = seaService.saveBooking(newBooking); //add booking to database
                    responseCode = 201;
                } else {
                    message = "Number of passengers must be 1 to 18 people.";
                    responseCode = 400;
                }
            } else {
                message = "System Error: When creating cutomer.";
                responseCode = 500;
            }
        } else {
            message = "Not all data found.";
            responseCode = 400;
        }
        return Response.status(responseCode).entity(message).build();
    }

    /**
     * Allows catch data to be added. User must send data in this order:
     * /dateOfTrip,timeOfTrip,notes,mackerelCount,codCount,sculpinCount,halibutCount,lobsterCount
     *
     * Date is set as YYYY-MM-DD or 2016-01-31. 
     * Time is set as 24-hour clock which is 00:00 to 23:59.
     *
     * @param userData
     * @return
     */
    @GET
    @Path("/addCatch/{param}")
    public Response addCatch(@PathParam("param") String userData) {
        int responseCode; //set response code
        String message; //set default message
        String[] catchData = userData.split(","); //get account data

        if (catchData.length == 8) {

            //get datetime, numberofpassengers for booking
            String datetime = catchData[0];
            String dateRegex = "(201[6-9]|202[0-9])-(0[1-9]|1[0-2])-([0-2][0-9]|3[0-2])";

            //validate date
            if (datetime.matches(dateRegex)) { 
                Date dateOfCatch = SeaUtil.convertString(datetime);
                Catchlog newCatch = new Catchlog(dateOfCatch, catchData[1], catchData[2], Integer.parseInt(catchData[3]), Integer.parseInt(catchData[4]), Integer.parseInt(catchData[5]), Integer.parseInt(catchData[6]),  Integer.parseInt(catchData[7]));
                message = seaService.saveCatch(newCatch);
                responseCode = 201;
            } else {
                message = "Catch log data wasn't correct.";
                responseCode = 400;
            }
        } else {
            message = "Catch log data is missing.";
            responseCode = 400;
        }
        return Response.status(responseCode).entity(message).build();
    }

    /**
     * Allows application to get booking data from database.
     *
     * @return
     */
    @GET
    @Path("/bookings")
    @Produces("application/json")
    public Response getBooking() {
        int responseCode;
        String jsonString;
        ArrayList<Bookings> bookings = seaService.getBookings();
        JSONObject json = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject object;
        try {

            if (bookings.isEmpty()) {
                jsonString = "No bookings";
                responseCode = 204;
            } else {
                for (Bookings booking : bookings) {
                    object = new JSONObject();
                    object.put("bookingId", booking.getBookingId());
                    object.put("numberOfPassengers", booking.getNumberOfPassengers());
                    object.put("dateBooked", booking.getDateBooked());
                    object.put("cost", booking.getCost());
                    object.put("customerId", booking.getCustomerId().getCustomerId());
                    array.put(object);
                }
                json.put("bookings", array);
                jsonString = json.toString();
                responseCode = 200;
            }
        } catch (JSONException jse) {
            System.out.println("Bookings Service Error: " + jse);
            jsonString = "System Error: Cannot create JSON object";
            responseCode = 500;
        }
        return Response.status(responseCode).entity(jsonString).build();
    }

    /**
     * Allows application to get customer data from database.
     *
     * @return
     */
    @GET
    @Path("/customers")
    @Produces("application/json")
    public Response getCustomers() {
        int responseCode; //set response code
        String jsonString; //set default message
        ArrayList<Customers> customers = seaService.getCustomers();
        JSONObject json = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject object;
        try {
            if (customers.isEmpty()) {
                jsonString = "No customers found.";
                responseCode = 204;
            } else {
                for (Customers customer : customers) {
                    object = new JSONObject();
                    object.put("customerId", customer.getCustomerId());
                    object.put("lastName", customer.getLastName());
                    object.put("firstName", customer.getFirstName());
                    object.put("creditCard", customer.getCreditCard());
                    object.put("email", customer.getEmail());
                    object.put("phone", customer.getPhone());
                    array.put(object);
                }
                json.put("customers", array);
                jsonString = json.toString();
                responseCode = 200;
            }
        } catch (JSONException jse) {
            System.out.println("Customers Service Error: " + jse);
            jsonString = "System Error: Cannot create JSON object";
            responseCode = 500;
        }
        return Response.status(responseCode).entity(jsonString).build();
    }

    /**
     * Allows user to get booking data from database.
     *
     * @return
     */
    @GET
    @Path("/financialreport")
    @Produces("application/json")
    public Response getReports() {
        int responseCode; //set response code
        String jsonString; //set default message
        ArrayList<FinancialReport> financialReports = SeaUtil.getReports();
        JSONObject json = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject object;
        try {

            if (financialReports.isEmpty()) {
                for (FinancialReport financialReport : financialReports) {
                    object = new JSONObject();
                    object.put("timePeriod", financialReport.getTimePeriod());
                    object.put("sales", financialReport.getSales());
                    object.put("totalPassengers", financialReport.getTotalPassengers());
                    array.put(object);
                }
                json.put("customers", array);
                jsonString = json.toString();
                responseCode = 200;
            } else {
                jsonString = "No financial reports found.";
                responseCode = 204;
            }
        } catch (JSONException jse) {
            System.out.println("Customers Service Error: " + jse);
            jsonString = "System Error: Cannot create JSON object";
            responseCode = 500;
        }
        return Response.status(responseCode).entity(jsonString).build();
    }

    @GET
    @Path("/catchlogs")
    @Produces("application/json")
    public Response getCatchLogs() {
        int responseCode; //set response code
        String jsonString; //set default message
        ArrayList<Catchlog> catchlogs = seaService.getCatchLogs();
        JSONObject json = new JSONObject();
        JSONArray array = new JSONArray();
        JSONObject object;
        try {
            if (catchlogs.isEmpty()) {
                jsonString = "No catchs found.";
                responseCode = 204;
            } else {
                for (Catchlog catchlog : catchlogs) {
                    object = new JSONObject();
                    object.put("id", catchlog.getId());
                    object.put("dateOfTrip", catchlog.getDateOfTrip());
                    object.put("timeOfTrip", catchlog.getTimeOfTrip());                  
                    object.put("codCount", catchlog.getCodCount());
                    object.put("lobsterCount", catchlog.getLobsterCount());
                    object.put("halibutCount", catchlog.getHalibutCount());
                    object.put("sculpinCount", catchlog.getSculpinCount());                
                    object.put("mackerelCount", catchlog.getMackerelCount());
                    object.put("notes", catchlog.getNotes());
                    array.put(object);
                }
                json.put("customers", array);
                jsonString = json.toString();
                responseCode = 200;
            }
        } catch (JSONException jse) {
            System.out.println("Customers Service Error: " + jse);
            jsonString = "System Error: Cannot create JSON object";
            responseCode = 500;
        }
        return Response.status(responseCode).entity(jsonString).build();
    }

    public Customers setCustomer(String creditCard, String lastName, String firstName, String phone, String email) {
        Customers newCustomer = new Customers();
        newCustomer.setCreditCard(creditCard);
        newCustomer.setLastName(lastName);
        newCustomer.setFirstName(firstName);
        newCustomer.setPhone(phone);
        newCustomer.setEmail(email);
        return newCustomer;
    }

    public boolean addCustomer(int customerId, Customers customer) {
        boolean added = false;
        Useraccess newuser = new Useraccess(); //create useraccess object
        newuser.setUsername(customer.getEmail()); //set customers email as username
        newuser.setPassword(customer.getPhone()); //set customers phone number as default password
        newuser.setUserAccessId(customerId); //set the id (key)
        newuser.setUserTypeCode(new Codevalue(2)); //set restrictions as customer by default
        newuser.setCustomers(customer); //add to useraccess object
        if (seaService.saveUser(newuser)) { //add useraccess to database
            if (seaService.saveCustomer(customer).equalsIgnoreCase("Customer was added.")) {//add customer to database
                added = true;
            }
        }
        return added;
    }
}
