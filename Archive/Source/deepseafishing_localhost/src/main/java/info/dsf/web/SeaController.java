package info.dsf.web;

import info.dsf.data.model.Bookings;
import info.dsf.data.model.Codevalue;
import info.dsf.data.model.Customers;
import info.dsf.data.model.Useraccess;
import info.dsf.service.SeaService;
import info.dsf.utils.SeaUtil;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Purpose: Directs server to correct page and adds needed information to each
 * page.
 */
@Controller
public class SeaController {

    private final SeaService seaService;

    @Autowired
    public SeaController(SeaService seaService) {
        this.seaService = seaService;
    }

    @RequestMapping("/memberLogin")
    public String loginMember(Model model, HttpSession session, HttpServletResponse response, @Valid @ModelAttribute("login") Useraccess login, BindingResult result) {

        /* Verify User Input */
        if (result.hasErrors()) {
            model.addAttribute("loginMessage", "Login failed");
            return "other/welcome";
        }

        /* Hit the web service to see if valid credentials. */
        String url = "http://localhost:8080/cisadmin/rest/useraccess/deepseafishing," + login.getUsername() + "," + login.getPassword();
        System.out.println("***URL For Validating: URL=" + url + "***");
        String results = seaService.loginMember(url);

        /* If successful add token and return to welcome.  Otherwise back to the login. */
        if (!results.equals("0")) {
            System.out.println("*** UserTypeCode = " + results + " ***");
            model.addAttribute("bookings", seaService.getBookings());
            model.addAttribute("userToken", results);
            session.setAttribute("userToken", results);
            session.setAttribute("username", login.getUsername());
            return "/fishing/bookings";
        } else {
            model.addAttribute("loginMessage", "Login failed");
            return "other/welcome";
        }
    }

    @RequestMapping("/logout")
    public String logoutMember(Model model, HttpSession session, @ModelAttribute("login") Useraccess login) {
        session.setAttribute("userToken", null);
        model.addAttribute("loginMessage", "Logged Out");
        return "other/welcome";
    }

    @RequestMapping("/bookings")
    public String showBookings(Model model, HttpSession session) {
        model.addAttribute("userToken", session.getAttribute("userToken"));
        model.addAttribute("bookings", seaService.getBookings());
        return "/fishing/bookings";
    }

    @RequestMapping("/addBooking")
    public String showAddBooking(Model model, HttpSession session, @ModelAttribute("customer") Customers customer) {
        //Gets user access token
        int userToken = getUserAccessToken(session);
        model.addAttribute("userToken", session.getAttribute("userToken"));

        //Allows user to access page base on user access token
        if (userToken == 1) {
            return "/fishing/admin/addBooking";
        }
        return "other/unauthorizedaccess";
    }

    @RequestMapping("/addbooking")
    public String addBooking(Model model, HttpSession session, @RequestParam("registrable") String registrable, @RequestParam("date") String date, @RequestParam("time") String time, @RequestParam("numberOfPeople") int numberOfPeople, @Valid @ModelAttribute("customer") Customers customer, BindingResult results) {
        //gets user access token
        int userToken = getUserAccessToken(session);
        model.addAttribute("userToken", session.getAttribute("userToken"));
        
        //get registerable boolean
        boolean bookable = false;
        if(registrable.equalsIgnoreCase("false")) {
            bookable = true;
        }

        //Allows user to access page base on user access token
        if (userToken == 1) {
            //Checks if jpa validation errors
            if (results.hasErrors()) {
                model.addAttribute("message", "Error booking trip.");
                return "/fishing/admin/addBooking";
            } else {

                String datetime = date + "T" + time;
                System.out.println("TESTING DATE OUTPUT: " + datetime);

                //Check if user is in database
                if (seaService.getCustomerByEmail(customer.getEmail().toLowerCase()).isEmpty()) {
                    int customerId = (seaService.getCustomers().size() + 1); //get current index              
                    Useraccess newuser = new Useraccess(); //create useraccess object
                    newuser.setUsername(customer.getEmail().toLowerCase()); //set customers email as username
                    newuser.setPassword(customer.getPhone().toLowerCase()); //set customers phone number as default password
                    newuser.setUserAccessId(customerId); //set the id (key)
                    newuser.setUserTypeCode(new Codevalue(2)); //set restrictions as customer by default
                    customer.setCustomerId(customerId); //update customer object
                    newuser.setCustomers(customer); //add to useraccess object
                    seaService.saveUser(newuser); //add useraccess to database
                    seaService.saveCustomer(customer); //add customer to database
                } else {
                    customer.setCustomerId(seaService.getCustomerByEmail(customer.getEmail().toLowerCase()).get(0).getCustomerId()); //update customer object with customers id
                }
                Bookings newBooking = new Bookings(); //create a booking object
                newBooking.setDateBooked(SeaUtil.convertStringDate(datetime)); //set event time
                newBooking.setRegistrable(bookable); //set event as private or not
                newBooking.setCost(SeaUtil.calculateCost(numberOfPeople)); //set cost
                newBooking.setNumberOfPassengers(numberOfPeople); //set number of people
                newBooking.setCustomerId(customer); //add customer information
                model.addAttribute("message", seaService.saveBooking(newBooking)); //add booking to database
                model.addAttribute("bookings", seaService.getBookings()); //get bookings from database
                return "/fishing/bookings";
            }
        }
        return "other/unauthorizedaccess";
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegister(Model model, HttpSession session, @RequestParam("bookingId") String bookingId, @ModelAttribute("customer") Customers customer) {
        //Gets user access token
        int userToken = getUserAccessToken(session);
        model.addAttribute("userToken", session.getAttribute("userToken"));

        //Allows user to access page base on user access token
        if (userToken == 1 || userToken == 2) {
            Object object = session.getAttribute("username");
            if (object != null) {
                String username = String.valueOf(object);
                System.out.println("TEST: " + username);
                customer = seaService.getCustomerByEmail(username.toLowerCase()).get(0); //get customer data              
                model.addAttribute("customer", customer); //pass view customer data
                model.addAttribute("bookingId", bookingId); //also pass the booking trip selected
                return "/fishing/admin/register";
            } else {
                model.addAttribute("message", "Error getting data."); //pass a error message
                model.addAttribute("bookings", seaService.getBookings()); //get bookings from database
                return "/fishing/bookings";
            }
        }
        return "other/unauthorizedaccess";
    }

    @RequestMapping("/addregister")
    public String addRegister(Model model, HttpSession session, @RequestParam("bookingId") int bookingId, @RequestParam("numberOfPassengers") int numberOfPeople, @ModelAttribute("customer") Customers customer) {
        //Gets user access token
        int userToken = getUserAccessToken(session);
        model.addAttribute("userToken", session.getAttribute("userToken"));

        //Allows user to access page base on user access token
        if (userToken == 1 || userToken == 2) {
            Object object = session.getAttribute("username");
            if (object != null) {
                String username = String.valueOf(object);
                customer = seaService.getCustomerByEmail(username.toLowerCase()).get(0); //set the customer object
                Bookings updateBooking = seaService.getBookingByBookingId(bookingId).get(0); //set the booking object
                int totalPeople = (updateBooking.getNumberOfPassengers() + numberOfPeople); //get totalPeople
                System.out.println("TEST: Registering " + customer.getEmail() + " for booking id " + updateBooking.getDateBooked() + ".");
                if (totalPeople > 18) { //check if totalPeople not over 18
                    System.out.println("ERROR: Too many people.");
                    model.addAttribute("message", "Too many people. Must be " + (18 - updateBooking.getNumberOfPassengers()) + " people.");
                } else {
                    System.out.println("TEST: Updating booking and creating a new one for " + customer.getEmail());
                    updateBooking.setNumberOfPassengers(totalPeople); //update booking
                    Bookings newBooking = new Bookings(); //create a booking object
                    newBooking.setDateBooked(updateBooking.getDateBooked()); //set date and time
                    newBooking.setNumberOfPassengers(totalPeople); //set number of people
                    newBooking.setCost(SeaUtil.calculateCost(numberOfPeople)); //set cost of booking
                    newBooking.setRegistrable(false); //set registerable
                    newBooking.setCustomerId(customer); //asign customer to booking
                    seaService.saveBooking(updateBooking); //update booking to database
                    model.addAttribute("message", seaService.saveBooking(newBooking)); //add new booking to database
                }
            } else {
                System.out.println("ERROR: Can't find object data.");
                model.addAttribute("message", "Error getting data.");
            }
            model.addAttribute("bookings", seaService.getBookings()); //get bookings from database
            return "/fishing/bookings";
        }
        return "other/unauthorizedaccess";
    }

    @RequestMapping("/customers")
    public String showCustomers(Model model, HttpSession session) {
        //Gets user access token
        int userToken = getUserAccessToken(session);
        model.addAttribute("userToken", session.getAttribute("userToken"));

        //Allows user to access page base on user access token
        if (userToken == 1) {
            model.addAttribute("customers", seaService.getCustomers());
            return "/fishing/admin/customers";
        }
        return "other/unauthorizedaccess";
    }

    @RequestMapping("/signup")
    public String showSignup(Model model, HttpSession session, @ModelAttribute("customer") Customers customer) {
        //Gets user access token
        int userToken = getUserAccessToken(session);
        model.addAttribute("userToken", session.getAttribute("userToken"));

        //Allows user to access page base on user access token
        if (userToken == 0 || userToken == 1) {
            return "/fishing/signup";
        }
        return "other/unauthorizedaccess";
    }

    @RequestMapping("/accountsignup")
    public String getSignup(Model model, HttpSession session, @Valid @ModelAttribute("customer") Customers customer, BindingResult results) {
        //Gets user access token
        int userToken = getUserAccessToken(session);
        model.addAttribute("userToken", session.getAttribute("userToken"));

        //Allows user to access page base on user access token
        if (userToken == 0 || userToken == 1) {
            String message = "Account already exist!";
            if (seaService.getCustomerByEmail(customer.getEmail().toLowerCase()).isEmpty()) { //checking if user exist
                int customerId = (seaService.getCustomers().size() + 1); //get current index              
                Useraccess newuser = new Useraccess(); //create useraccess object
                newuser.setUsername(customer.getEmail().toLowerCase()); //set customers email as username
                newuser.setPassword(customer.getPhone().toLowerCase()); //set customers phone number as default password
                newuser.setUserAccessId(customerId); //set the id (key)
                newuser.setUserTypeCode(new Codevalue(2)); //set restrictions as customer by default
                customer.setCustomerId(customerId); //update customer object
                newuser.setCustomers(customer); //add to useraccess object
                seaService.saveUser(newuser); //add useraccess to database
                message = seaService.saveCustomer(customer); //add customer to database
            }
            model.addAttribute("message", message);
            return "/fishing/signup";
        }
        return "other/unauthorizedaccess";
    }

    @RequestMapping("/financialreport")
    public String showFinancialReport(Model model, HttpSession session) {
        //Gets user access token
        int userToken = getUserAccessToken(session);
        model.addAttribute("userToken", session.getAttribute("userToken"));

        //Allows user to access page base on user access token
        if (userToken == 1) {
            model.addAttribute("reports", SeaUtil.getReports());
            return "/fishing/reports/financialreport";
        }
        return "other/unauthorizedaccess";
    }

    public int getUserAccessToken(HttpSession session) {
        Object token = session.getAttribute("userToken"); //get usertoken from session
        int userToken = 0; //set a default value for token
        if (token != null) { //check if object null
            userToken = Integer.parseInt(session.getAttribute("userToken").toString()); //convert object to int if not null
        }
        return userToken; //pass the token to controller
    }
}
